/*****************************************************************************/
/*!	\file sysctype.h
 *	\brief SystemCORP definition file
	\par        Dalian Yunxing Tech Co., Ltd.

				Dalian, China
				Phone   : +86 (411) 8825 4852
				Email   : yx@yunxing.tech
 */
/*****************************************************************************/
#ifndef SYSCTYPE_H
#define SYSCTYPE_H 1

struct StringList {
	unsigned int  uiLen;
	char** list;
};

	/* Define TRUE and FALSE if not defined */
	#ifndef FALSE
	/*! \brief FALSE if not defined */
		#define FALSE	0
	#endif
	#ifndef TRUE
	/*! \brief TRUE if not defined */
		#define TRUE	1
	#endif

	/*!\brief BYTES_NONE 0 bytes size*/
	#define BYTES_NONE 		0
	/*!\brief BYTES_64 64 bytes size*/
	#define BYTES_64 		64
	/*!\brief BYTES_128 128 bytes size*/
	#define BYTES_128 		128
	/*!\brief BYTES_256 256 bytes size*/
	#define BYTES_256 		256
	/*!\brief KILOBYTE 1024 bytes size*/
	#define KILOBYTE 		1024
	/*!\brief KILOBYTES_2 2048 bytes size*/
	#define KILOBYTES_2 		2048

#if !defined(TGTTYPES_H) && !defined(SETTARGET_TYPES_H)

#ifdef BORLAND_BOOLEAN
	/*!	\brief Size : 8 bits	Range : TRUE or FALSE */
	#define Boolean				bool
#else
	/*!	\brief Size : 8 bits	Range : TRUE or FALSE */
        typedef unsigned char 		Boolean;
#endif // BORLAND_BOOLEAN

#if  defined(WIN32) && (! defined(_NI_mswin32_) )
	/*!	\brief Size : 8 bits	Range : 0 to 255 */
	typedef unsigned char 			Unsigned8;
	/*!	\brief Size : 8 bits	Range : -128 to 127 */
	typedef signed char 			Integer8;
	/*!	\brief Size : 8 bits	Range : 0 to 255 */
	typedef unsigned char 			Byte;
	/*!	\brief Size : 16 bits	Range : 0 to 65,535  */
	typedef unsigned  short  int	Unsigned16;
	/*!	\brief Size : 16 bits	Range : -32,768 to 32,767 */
	typedef signed short int 		Integer16;
	/*!	\brief Size : 32 bits	Range : 0 to 4,294,967,295 */
	typedef unsigned int			Unsigned32;
	/*!	\brief Size : 32 bits	Range : -2,147,483,648 to 2,147,483,647 */
	typedef signed int 				Integer32;
	/*!	\brief Size : 64 bits	Range : 0 to 18,446,744,073,709,551,615 */
	typedef unsigned long long 		Unsigned64;
	/*!	\brief Size : 64 bits	Range : ?,223,372,036,854,775,808 to 9,223,372,036,854,775,807 */
	typedef signed long long		Integer64;

#elif  defined(_NI_mswin32_)
	/*!	\brief Size : 8 bits	Range : 0 to 255 */
	typedef unsigned char 			Unsigned8;
	/*!	\brief Size : 8 bits	Range : -128 to 127 */
	typedef signed char 			Integer8;
	/*!	\brief Size : 8 bits	Range : 0 to 255 */
	typedef unsigned char 			Byte;
	/*!	\brief Size : 16 bits	Range : 0 to 65,535  */
	typedef unsigned  short  int	Unsigned16;
	/*!	\brief Size : 16 bits	Range : -32,768 to 32,767 */
	typedef signed short int 		Integer16;
	/*!	\brief Size : 32 bits	Range : 0 to 4,294,967,295 */
	typedef unsigned int			Unsigned32;
	/*!	\brief Size : 32 bits	Range : -2,147,483,648 to 2,147,483,647 */
	typedef signed int 				Integer32;
	/*!	\brief Size : 64 bits	Range : 0 to 18,446,744,073,709,551,615 */
	typedef unsigned __int64 		Unsigned64;
	/*!	\brief Size : 64 bits	Range : ?,223,372,036,854,775,808 to 9,223,372,036,854,775,807 */
	typedef signed __int64			Integer64;
#elif defined(__linux__) || defined(__CHRONOS__) || defined(_SC1x5_)
	#include	<stdint.h>
	/*!	\brief Size : 8 bits	Range : 0 to 255 */
	typedef uint8_t                 Unsigned8;
	/*!	\brief Size : 8 bits	Range : -128 to 127 */
	typedef int8_t                  Integer8;
	/*!	\brief Size : 8 bits	Range : 0 to 255 */
	typedef unsigned char 			Byte;
	/*!	\brief Size : 16 bits	Range : 0 to 65,535  */
	typedef uint16_t                Unsigned16;
	/*!	\brief Size : 16 bits	Range : -32,768 to 32,767 */
	typedef int16_t                 Integer16;
	/*!	\brief Size : 32 bits	Range : 0 to 4,294,967,295 */
	typedef uint32_t				Unsigned32;
	/*!	\brief Size : 32 bits	Range : -2,147,483,648 to 2,147,483,647 */
	typedef int32_t 				Integer32;
	/*!	\brief Size : 64 bits	Range : 0 to 18,446,744,073,709,551,615 */
	typedef uint64_t				Unsigned64;
	/*!	\brief Size : 64 bits	Range : ?,223,372,036,854,775,808 to 9,223,372,036,854,775,807 */
	typedef int64_t 				Integer64;
#elif defined(VXWORX_ACE)
	/*!	\brief Size : 8 bits	Range : 0 to 255 */
	typedef unsigned char	 		Unsigned8;
	/*!	\brief Size : 8 bits	Range : -128 to 127 */
	typedef signed char				Integer8;
	/*!	\brief Size : 8 bits	Range : 0 to 255 */
	typedef unsigned char 			Byte;
	/*!	\brief Size : 16 bits	Range : 0 to 65,535  */
	typedef unsigned short			Unsigned16;
	/*!	\brief Size : 16 bits	Range : -32,768 to 32,767 */
	typedef signed short			Integer16;
	/*!	\brief Size : 32 bits	Range : 0 to 4,294,967,295 */
	typedef unsigned int			Unsigned32;
	/*!	\brief Size : 32 bits	Range : -2,147,483,648 to 2,147,483,647 */
	typedef signed int 				Integer32;
	/*!	\brief Size : 64 bits	Range : 0 to 18,446,744,073,709,551,615 */
	typedef unsigned long long		Unsigned64;
	/*!	\brief Size : 64 bits	Range : ?,223,372,036,854,775,808 to 9,223,372,036,854,775,807 */
	typedef signed long long		Integer64;
#elif defined(WIN64)
		/*!	\brief Size : 8 bits	Range : 0 to 255 */
		typedef unsigned char 			Unsigned8;
		/*!	\brief Size : 8 bits	Range : -128 to 127 */
		typedef signed char 			Integer8;
		/*!	\brief Size : 8 bits	Range : 0 to 255 */
		typedef unsigned char 			Byte;
		/*!	\brief Size : 16 bits	Range : 0 to 65,535  */
		typedef unsigned  short  int	Unsigned16;
		/*!	\brief Size : 16 bits	Range : -32,768 to 32,767 */
		typedef signed short int 		Integer16;
		/*!	\brief Size : 32 bits	Range : 0 to 4,294,967,295 */
		typedef unsigned int			Unsigned32;
		/*!	\brief Size : 32 bits	Range : -2,147,483,648 to 2,147,483,647 */
		typedef signed int 				Integer32;
		/*!	\brief Size : 64 bits	Range : 0 to 18,446,744,073,709,551,615 */
		typedef unsigned __int64 		Unsigned64;
		/*!	\brief Size : 64 bits	Range : ?,223,372,036,854,775,808 to 9,223,372,036,854,775,807 */
		typedef signed __int64			Integer64;
#else
	/*!	\brief Size : 8 bits	Range : 0 to 255 */
	typedef unsigned char 			Unsigned8;
	/*!	\brief Size : 8 bits	Range : -128 to 127 */
	typedef signed char 			Integer8;
	/*!	\brief Size : 8 bits	Range : 0 to 255 */
	typedef unsigned char 			Byte;
	/*!	\brief Size : 16 bits	Range : 0 to 65,535  */
	typedef unsigned   int			Unsigned16;
	/*!	\brief Size : 16 bits	Range : -32,768 to 32,767 */
	typedef signed  int 			Integer16;
	/*!	\brief Size : 32 bits	Range : 0 to 4,294,967,295 */
	typedef unsigned long int		Unsigned32;
	/*!	\brief Size : 32 bits	Range : -2,147,483,648 to 2,147,483,647 */
	typedef signed long int 			Integer32;
#endif
#if defined(__PARADIGM__)
	typedef Unsigned32 Unsigned64;
	typedef Integer32 Integer64;
#endif
	/*!	\brief Size : 32 bits	Range : Exponent 8 bits, Fraction 32 bits of precision */
	typedef float				Float32;
	/*!	\brief Size : 64 bits	Range : Exponent 11 bits, Fraction 52 bits of precision */
	typedef double				Float64;
	/*!	\brief Size : 80 bits	Range : Exponent 15 bits, Fraction 63 bits of precision */
	typedef long double			Float80;
#endif // !defined(TGTTYPES_H) && !defined(SETTARGET_TYPES_H)
#endif // SYSCTYPE_H
// End of File
