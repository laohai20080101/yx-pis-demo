﻿project(demo)

cmake_minimum_required(VERSION 2.8)
message("Project Directory: ${PROJECT_SOURCE_DIR}")
include_directories(exampleApplications/examples/header)
include_directories(common/header)

#Linux
IF (CMAKE_SYSTEM_NAME MATCHES "Linux")

set(ARCH "AMD64" CACHE STRING "Target Arch")

add_definitions(-D USE_SYSCTYPE)
add_definitions(-D __linux__)
add_definitions(-D SETTARGET_DEVICE_GENERIC_LINUX)
add_definitions(-D SETTARGET_DEBUG_ENABLED=0)

set(ARCH "AMD64" CACHE STRING "Target Arch")
IF (ARCH STREQUAL "ARM")
  include_directories(/XXX/include)
  set(CMAKE_C_COMPILER "/XXX/bin/arm-linux-gnueabi-gcc")
  set(CMAKE_CXX_COMPILER "/XXX/bin/arm-linux-gnueabi-g++")
ELSE (ARCH STREQUAL "AMD64")
  include_directories(/usr/include/x86_64-linux-gnu)
  set(CMAKE_C_COMPILER "gcc")
  set(CMAKE_CXX_COMPILER "g++")
  LINK_DIRECTORIES(${CMAKE_CURRENT_SOURCE_DIR}/lib/linux_x64)
ENDIF (ARCH STREQUAL "ARM")

add_executable(example-server exampleApplications/examples/source/IEC61850Functions.c
                      exampleApplications/examples/source/LocalData.c
                      exampleApplications/examples/source/MainServer.c
                      exampleApplications/examples/source/PIS10CreateServerClient.c
                      exampleApplications/examples/source/PIS10Callbacks.c
                      exampleApplications/examples/source/PrintView.c
                      exampleApplications/examples/source/UserInput.c)
target_link_libraries(example-server libyxpis.a pthread pcap rt m dl)


add_executable(example-client exampleApplications/examples/source/IEC61850Functions.c
                      exampleApplications/examples/source/LocalData.c
                      exampleApplications/examples/source/MainClient.c
                      exampleApplications/examples/source/PIS10CreateServerClient.c
                      exampleApplications/examples/source/PIS10Callbacks.c
                      exampleApplications/examples/source/PrintView.c
                      exampleApplications/examples/source/UserInput.c)
target_link_libraries(example-client libyxpis.a pthread pcap rt m dl)

#Windows
ELSEIF (CMAKE_SYSTEM_NAME MATCHES "Windows")

add_definitions(-D _WINDOWS)
add_definitions(-D SETTARGET_DEVICE_GENERIC_WINDOWS)
add_definitions(-D SETTARGET_DEBUG_ENABLED=0)

IF(CMAKE_SIZEOF_VOID_P EQUAL 8)
LINK_DIRECTORIES(${CMAKE_CURRENT_SOURCE_DIR}/lib/win_x64)
add_definitions(-D WIN64)
ELSE(CMAKE_SIZEOF_VOID_P EQUAL 8)
LINK_DIRECTORIES(${CMAKE_CURRENT_SOURCE_DIR}/lib/win32)
add_definitions(-D WIN32)
ENDIF(CMAKE_SIZEOF_VOID_P EQUAL 8)

add_executable(example-server exampleApplications/examples/source/IEC61850Functions.c
                      exampleApplications/examples/source/LocalData.c
                      exampleApplications/examples/source/MainServer.c
                      exampleApplications/examples/source/PIS10CreateServerClient.c
                      exampleApplications/examples/source/PIS10Callbacks.c
                      exampleApplications/examples/source/PrintView.c
                      exampleApplications/examples/source/UserInput.c)
target_link_libraries(example-server yxpis)

add_executable(example-client exampleApplications/examples/source/IEC61850Functions.c
                      exampleApplications/examples/source/LocalData.c
                      exampleApplications/examples/source/MainClient.c
                      exampleApplications/examples/source/PIS10CreateServerClient.c
                      exampleApplications/examples/source/PIS10Callbacks.c
                      exampleApplications/examples/source/PrintView.c
                      exampleApplications/examples/source/UserInput.c)
target_link_libraries(example-client yxpis)

ENDIF (CMAKE_SYSTEM_NAME MATCHES "Linux")


