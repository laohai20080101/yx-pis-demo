# YX-PIS-Demo

#### 介绍
YX-PIS IEC61850协议栈demo示例。

#### 说明
本demo提供一个YX-PIS的C语言应用示例，以有限制lib的方式提供Windows、Linux的全功能核心协议栈。
用户可获取本demo源码，利用协议栈API实现各种功能测试或研发摸底。

本Demo目前仅支持Windows和Ubuntu18.04 64位平台。请勿在其他平台试用。

#### Demo安装/编译教程

Windows：
1.  安装vs2019（或以上），并且支持c/c++
2.  双击exampleApplications\examples\project\Windows下的 IEC61850_Example.sln
3.  编译成功后，请将lib\win32下的yxpis.dll拷贝到exampleApplications\output，以支持程序的运行
4.  如果本机没有安装winpcap，请安装winpcap4.1.3版本

Linux：
1.  cd exampleApplications
2.  mkdir build
3.  cd build
4.  cmake ../..
5.  make
6.  如果本机未安装libpcap，请安装

#### 安装YX-PIS运行依赖
默认情况下，YX-PIS的运行需要按照依赖库。目前在windows下需要Winpcap，在linux系统下需要libpcap，无其他依赖。

##### **Windows下安装**
可从网上下载获取安装包：WinPcap_4_1_3.exe
直接双击运行按提示安装即可。

##### **Linux下安装**
请下载获取源码：libpcap-1.10.1.tar.gz

CentOS、红帽子等系统的常用安装方法参考：

1.安装GCC：

    yum -y install gcc-c++

2.安装flex： 

    yum -y install flex 

3.安装bison

    yum -y install bison

4.安装 libpcap

解压软件包：tar zxf libpcap-1.10.1.tar.gz
进入解压后的目录，输入命令行：
命令如下：

     ./configure
     make
     make install

Ubuntu的常用安装方法参考：

1.先安装GCC 

    sudo apt-get install gcc

2.安装build-essential

    sudo apt-get install build-essential 

3.安装m4

    sudo apt-get install m4

或者：

GNU M4可以从此处 ftp.gnu.org/gnu/m4/ 下载 

    sudo tar -zxvf m4-latest.tar.gzcd m4-1.4.17
    sudo ./configure
    sudo make
    sudo make install 

4.安装 flex 

    sudo apt-get install flex 

5.安装 bison 

    sudo apt-get install bison 

6.安装 libpcap 

    tar -zxvf libpcap-1.10.1.tar.gz
    cd libpcap-1.10.1
    sudo ./configure
    sudo make
    sudo make install 

#### Demo运行

1. 如果编译没有问题，可以尝试执行Demo，执行前请先修改CID文件的IP相关信息。

（服务端与客户端允许运行在同一个设备上）

修改Server.cid：

   ```xml
           <Address>
              <P type="OSI-AP-Title">1,1,9999,1</P>
              <P type="OSI-AE-Qualifier">12</P>
              <P type="OSI-PSEL">00000001</P>
              <P type="OSI-SSEL">0001</P>
              <P type="OSI-TSEL">0001</P>
              <P type="IP">192.168.8.1</P>
              <!-- 改成本机的IP位置 -->
              <P type="IP-SUBNET">255.255.255.0</P>
              <P type="IP-GATEWAY">192.168.8.2</P>
   		   <P type="MAC-Address">00-50-56-C0-00-08</P>
              <!-- 改成本机的MAC地址（虚拟机需要设置，非虚拟机可以删除本行） -->
           </Address>
           <GSE cbName="GooseCB" ldInst="Example">
              <Address>
                 <P type="MAC-Address">01-0C-CD-01-00-01</P>
                 <P type="APPID">0001</P>
                 <P type="VLAN-PRIORITY">4</P>
                 <P type="VLAN-ID">000</P>
              </Address>
              <MinTime multiplier="m" unit="s">400</MinTime>
              <MaxTime multiplier="m" unit="s">4000</MaxTime>
           </GSE>
   ```

修改Client.cid：

   ```xml
     <SubNetwork name="SubNetworkName">
        <ConnectedAP apName="SubstationRing1" iedName="ServerIED">
           <Address>
              <P type="OSI-AP-Title">1,1,9999,1</P>
              <P type="OSI-AE-Qualifier">12</P>
              <P type="OSI-PSEL">00000001</P>
              <P type="OSI-SSEL">0001</P>
              <P type="OSI-TSEL">0001</P>
              <P type="IP">192.168.8.1</P>
              <!-- 设置为服务端的IP地址 -->
              <P type="IP-SUBNET">255.255.255.0</P>
              <P type="IP-GATEWAY">192.168.8.2</P>
              <P type="MAC-Address">00-50-56-C0-00-08</P>
              <!-- 设置为服务端的MAC地址 -->
           </Address>
           <GSE cbName="GooseCB" ldInst="Example">
              <Address>
                 <P type="MAC-Address">01-0C-CD-01-00-01</P>
                 <P type="APPID">0001</P>
                 <P type="VLAN-PRIORITY">4</P>
                 <P type="VLAN-ID">000</P>
              </Address>
              <MinTime multiplier="m" unit="s">400</MinTime>
              <MaxTime multiplier="m" unit="s">4000</MaxTime>
           </GSE>
        </ConnectedAP>
        <ConnectedAP apName="SubStationRing2" iedName="ClientIED">
           <Address>
              <P type="OSI-AP-Title">1,1,9999,1</P>
              <P type="OSI-AE-Qualifier">12</P>
              <P type="OSI-PSEL">00000001</P>
              <P type="OSI-SSEL">0001</P>
              <P type="OSI-TSEL">0001</P>
              <P type="IP">192.168.8.1</P>
              <!-- 设置为本机的IP地址 -->
              <P type="IP-SUBNET">255.255.255.0</P>
              <P type="IP-GATEWAY">192.168.8.2</P>
              <P type="MAC-Address">00-50-56-C0-00-08</P>
              <!-- 设置为本机的MAC地址 -->
           </Address>
        </ConnectedAP>
     </SubNetwork>
   ```

2. Linux下需要使用root权限运行（sudo）。

其他平台：

请咨询云行科技技术支持。

#### 参考资料

参考本站wiki：https://gitee.com/yx-pis/yx-pis-demo/wikis/YX-PIS

更多相关咨询，欢迎访问：http://yunxing.tech

或请联络：[yx@yunxing.tech](mailto://yx@yunxing.tech)