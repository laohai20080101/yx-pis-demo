/******************************************************************************
*
	\par        Dalian Yunxing Tech Co., Ltd.

				Dalian, China
				Phone   : +86 (411) 8825 4852
				Email   : yx@yunxing.tech
*******************************************************************************/

/*****************************************************************************/
/*!	\file		MainClient.h
 *	\brief 		Header file for the PIS10 Stack Example
	\par        Dalian Yunxing Tech Co., Ltd.

				Dalian, China
				Phone   : +86 (411) 8825 4852
				Email   : yx@yunxing.tech
 */
/*****************************************************************************/

/******************************************************************************
*	Defines
******************************************************************************/

#ifndef MAIN_CLIENT_INCLUDED
#define MAIN_CLIENT_INCLUDED 1

/******************************************************************************
*	Includes
******************************************************************************/

#include <stdio.h>
#include <stdlib.h>

#include "IEC61850Types.h"
#include "IEC61850API.h"
#include "sysctype.h"

#include "PIS10CreateServerClient.h"
#include "IEC61850Functions.h"
#include "LocalData.h"
#include "ExampleTypes.h"
#include "PrintView.h"
#include "UserInput.h"

/******************************************************************************
*	Prototypes
******************************************************************************/

enum IEC61850_ErrorCodes setupIEC61850Client(void);

#endif
