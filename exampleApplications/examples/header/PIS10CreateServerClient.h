/******************************************************************************
*
	\par        Dalian Yunxing Tech Co., Ltd.

				Dalian, China
				Phone   : +86 (411) 8825 4852
				Email   : yx@yunxing.tech
*******************************************************************************/

/*****************************************************************************/
/*!	\file		PIS10CreateServerClient.h
 *	\brief 		C Source code file for the PIS10 Stack Example
	\par        Dalian Yunxing Tech Co., Ltd.

				Dalian, China
				Phone   : +86 (411) 8825 4852
				Email   : yx@yunxing.tech
 */
/*****************************************************************************/

/******************************************************************************
*	Defines
******************************************************************************/


#ifndef CLIENT_SERVER_FUNCTIONS_INCLUDED
#define CLIENT_SERVER_FUNCTIONS_INCLUDED 1

/******************************************************************************
*	Includes
******************************************************************************/
#include <stdio.h>
#include <stdlib.h>

#include "IEC61850Types.h"
#include "IEC61850API.h"
#include "sysctype.h"

#include "PIS10Callbacks.h"

/******************************************************************************
*	Prototypes
******************************************************************************/

enum IEC61850_ErrorCodes CreateServer(unsigned long int uiOptions);

enum IEC61850_ErrorCodes CreateClient(void);

IEC61850 GetMyServerClient(void);

int IsMyServerClientNULL(void);

enum IEC61850_ErrorCodes  LoadSCLFile(char *pPath);

enum IEC61850_ErrorCodes StartMyServerClient(void);

enum IEC61850_ErrorCodes StopMyServerClient(void);

enum IEC61850_ErrorCodes FreeMyServerClient(void);

enum IEC61850_ErrorCodes SetUserData(void * inData);

enum IEC61850_ErrorCodes SetControlsOrCat();

#endif
