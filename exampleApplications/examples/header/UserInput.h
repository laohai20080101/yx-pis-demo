/******************************************************************************
*
	\par        Dalian Yunxing Tech Co., Ltd.

				Dalian, China
				Phone   : +86 (411) 8825 4852
				Email   : yx@yunxing.tech
*******************************************************************************/

/*****************************************************************************/
/*!	\file		UserInput.h
 *	\brief 		Header file for the PIS10 Stack Example
	\par        Dalian Yunxing Tech Co., Ltd.

				Dalian, China
				Phone   : +86 (411) 8825 4852
				Email   : yx@yunxing.tech
 */
/*****************************************************************************/

/******************************************************************************
*	Defines
******************************************************************************/

#ifndef USER_INPUT_INCLUDED
#define USER_INPUT_INCLUDED 1

/******************************************************************************
*	Includes
******************************************************************************/

#include <stdio.h>
#include "ExampleTypes.h"
#include "LocalData.h"

/******************************************************************************
*	Prototypes
******************************************************************************/
enum UserCommands GetServerCommandFromUser(void);
enum UserCommands GetClientCommandFromUser(void);
Boolean GetBooleanFromUser(void);
Integer32 GetInteger32FromUser(void);
Integer32 GetCommIndexFromUser(void);
Integer8 GetIndexForArray(void);
Float32 GetFloat32FromUser(void);
struct IEC61850_TimeStamp* GetIEC61850TimeFromUser(void);

#endif
