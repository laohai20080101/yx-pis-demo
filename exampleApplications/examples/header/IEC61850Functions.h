/******************************************************************************
*
	\par        Dalian Yunxing Tech Co., Ltd.

				Dalian, China
				Phone   : +86 (411) 8825 4852
				Email   : yx@yunxing.tech
*******************************************************************************/

/*****************************************************************************/
/*!	\file		IEC61850Functions.h
*	\brief 		C header file for the PIS10 Stack Example
	\par        Dalian Yunxing Tech Co., Ltd.

				Dalian, China
				Phone   : +86 (411) 8825 4852
				Email   : yx@yunxing.tech
*/
/*****************************************************************************/

/******************************************************************************
*	Defines
******************************************************************************/


#ifndef IEC61850_FUNCTIONS_INCLUDED
#define IEC61850_FUNCTIONS_INCLUDED 1



/******************************************************************************
*	Includes
******************************************************************************/

#include <stdio.h>

#include "IEC61850Types.h"
#include "IEC61850API.h"
#include "sysctype.h"

#include "LocalData.h"
#include "PIS10CreateServerClient.h"

/******************************************************************************
*	Prototypes
******************************************************************************/
enum IEC61850_ErrorCodes SelectCSWI(Boolean inCtrlValue);
enum IEC61850_ErrorCodes OperateCSWI(Boolean inCtrlValue);

enum IEC61850_ErrorCodes SelectGGIO(Boolean inCtrlValue);
enum IEC61850_ErrorCodes OperateGGIO(Boolean inCtrlValue);

enum IEC61850_ErrorCodes UpdateMMXUPhsAMagi(Integer32 inUpdateValue);
enum IEC61850_ErrorCodes UpdateMMXUPhsBMagi(Integer32 inUpdateValue);
enum IEC61850_ErrorCodes UpdateMMXUPhsCMagi(Integer32 inUpdateValue);

enum IEC61850_ErrorCodes UpdateTTNSTnsSv1Magf(Float32 inUpdateValue);
enum IEC61850_ErrorCodes UpdateTTNSTnsSv2Magf(Float32 inUpdateValue);
enum IEC61850_ErrorCodes UpdateTTNSTnsSv3Magf(Float32 inUpdateValue);

enum IEC61850_ErrorCodes UpdatePDIFCrvPts(CrvPts inUpdateValue, Integer8 inIndex);

enum IEC61850_ErrorCodes MMSGetConnectionsList(void);
enum IEC61850_ErrorCodes MMSGetFileAttribs(void);
enum IEC61850_ErrorCodes MMSGetFile(void);
enum IEC61850_ErrorCodes SetFile();
enum IEC61850_ErrorCodes MMSDeleteFile(void);

enum IEC61850_ErrorCodes QueryLogByTime();
enum IEC61850_ErrorCodes QueryLogAfterEnerID();
enum IEC61850_ErrorCodes GetLogStatusValues();

enum IEC61850_ErrorCodes GetDataSetDirectory();
enum IEC61850_ErrorCodes MMSReadOrWrite();
enum IEC61850_ErrorCodes MMS_Read();
enum IEC61850_ErrorCodes MMS_Write();

enum IEC61850_ErrorCodes GetSGValues();
enum IEC61850_ErrorCodes SelectActiveSG();
enum IEC61850_ErrorCodes SelectEditSG();
enum IEC61850_ErrorCodes SetEditSG();
enum IEC61850_ErrorCodes ConfirmEditSG();
enum IEC61850_ErrorCodes GetSGCBValues();

void FileOperate();
void SittingGroupOperate();
void LogOperate();

#endif
