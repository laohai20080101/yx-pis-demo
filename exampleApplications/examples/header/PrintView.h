/******************************************************************************
*
	\par        Dalian Yunxing Tech Co., Ltd.

				Dalian, China
				Phone   : +86 (411) 8825 4852
				Email   : yx@yunxing.tech
*******************************************************************************/

/*****************************************************************************/
/*!	\file		PrintView.h
 *	\brief 		Header file for the PIS10 Stack Example
	\par        Dalian Yunxing Tech Co., Ltd.

				Dalian, China
				Phone   : +86 (411) 8825 4852
				Email   : yx@yunxing.tech
 */
/*****************************************************************************/

/******************************************************************************
*	Defines
******************************************************************************/

#ifndef PRINT_VIEW_INCLUDED
#define PRINT_VIEW_INCLUDED 1

/******************************************************************************
*	Includes
******************************************************************************/

#include <stdio.h>
#include "IEC61850API.h"
#include "sysctype.h"
#include "ExampleTypes.h"

#include "LocalData.h"

/******************************************************************************
*	Prototypes
******************************************************************************/

void PrintServerFullView(void);
void PrintClientFullView(void);
void PrintServerSubscriptionFullView(void);

void PrintServerHeader(void);
void PrintClientHeader(void);
void PrintServerSubscriptionHeader(void);

void PrintDataView(void);
void PrintServerSubscriptionDataView(void);

void PrintErrorString(void);
void PrintServerMenuView(void);
void PrintClientMenuView(void);
void PrintServerSubscriptionMenuView(void);

const char* BooleanToString(Boolean inBool);
const char* DBPosToString(enum DbPosValues inDBPosVal);
void ClearScreen(void);

void PrintFileMenuView(void);
void PrintSettingGroupMenuView(void);

void PrintLogMenuView(void);


void printTimeStamp(struct IEC61850_TimeStamp* pTimeStamp);
void printLogVals(struct IEC61850_LogEntries* ptLogEntries);
void printLogStatus(struct IEC61850_LogStatus* ptResultList);
void printDAVal(struct IEC61850_DataAttributeData* ptDataAttribute, int level);

#endif
