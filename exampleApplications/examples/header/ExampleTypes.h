/******************************************************************************
*
	\par        Dalian Yunxing Tech Co., Ltd.

				Dalian, China
				Phone   : +86 (411) 8825 4852
				Email   : yx@yunxing.tech
*******************************************************************************/

/*****************************************************************************/
/*!	\file		PrintView.h
 *	\brief 		Header file for the PIS10 Stack Example
	\par        Dalian Yunxing Tech Co., Ltd.

				Dalian, China
				Phone   : +86 (411) 8825 4852
				Email   : yx@yunxing.tech
 */
/*****************************************************************************/

#ifndef EXAMPLE_TYPES_INCLUDED
#define EXAMPLE_TYPES_INCLUDED 1

#include "sysctype.h"

/******************************************************************************
*	Defines
******************************************************************************/

enum ExampleErrorCodes{
	EXAMPLE_NO_ERROR 		 		 =  0,
	EXAMPLE_INVALID_PARAMETERS 		 = -1,
	EXAMPLE_INVALID_USER_INPUT 		 = -2,
	EXAMPLE_INDEX_OUT_OF_BOUNDS		 = -2,
	EXAMPLE_CALLOC_FAILED			 = -3,
};

enum UserCommands {
	COMMAND_NONE	                      = 0,
	COMMAND_EXIT	                      = 1,
	COMMAND_READ_ALL                      = 2,
	COMMAND_OPERATE_CSWI	              = 3,
	COMMAND_OPERATE_GGIO	              = 4,
	COMMAND_UPDATE_MMXU_PHSA	          = 5,
	COMMAND_UPDATE_MMXU_PHSB	          = 6,
	COMMAND_UPDATE_MMXU_PHSC	          = 7,
	COMMAND_UPDATE_PDIF_XVAL	          = 8,
	COMMAND_UPDATE_PDIF_YVAL	          = 9,
	COMMAND_MMS_GET_CONNECTED_SERVER_LIST = 10,
	COMMAND_GET_DATASET_DIR               = 11,
	COMMAND_MMS_READ_AND_WRITE            = 12,
	COMMAND_MMS_QUERY_LOG                 = 13,
	COMMAND_PRINT_FILE_MENU               = 14,
	COMMAND_PRINT_SETTING_GROUP_MENU      = 15,
	COMMAND_UPDATE_TTNS_TNSSV1            = 16,
	COMMAND_UPDATE_TTNS_TNSSV2            = 17,
	COMMAND_UPDATE_TTNS_TNSSV3            = 18,
};


/* Define Structure to user for the CrvPts array */
typedef struct{
	Float32 xVal;
	Float32 yVal;
}CrvPts;

#define NUM_CRVPTS_STRUCT_ELEMENTS 2  //The Number of Elements in the Structure holding the CrvPts
/******************************************************************************
*	Includes
******************************************************************************/

#include <stdio.h>

/******************************************************************************
*	Prototypes
******************************************************************************/



#endif
