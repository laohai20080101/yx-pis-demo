/******************************************************************************
*
	\par        Dalian Yunxing Tech Co., Ltd.

				Dalian, China
				Phone   : +86 (411) 8825 4852
				Email   : yx@yunxing.tech
*******************************************************************************/

/*****************************************************************************/
/*!	\file		LocalData.h
 *	\brief 		Header file for the PIS10 Stack Example
	\par        Dalian Yunxing Tech Co., Ltd.

				Dalian, China
				Phone   : +86 (411) 8825 4852
				Email   : yx@yunxing.tech
 */
/*****************************************************************************/

/******************************************************************************
*	Defines
******************************************************************************/

#ifndef LOCAL_DATA_INCLUDED
#define LOCAL_DATA_INCLUDED 1

#define SIZE_OF_PDIF_ARRAY 6
#define SIZE_OF_ERROR_STRING 255

/* Needed to make snprintf work in Visual Studio */
#ifdef _MSC_VER
	#if _MSC_VER
	   #define snprintf _snprintf
	#endif
#endif
/******************************************************************************
*	Includes
******************************************************************************/

#include <stdio.h>
#include "IEC61850Types.h"
#include "sysctype.h"

#include "ExampleTypes.h"

/******************************************************************************
*	Prototypes
******************************************************************************/
/* CSWI */
Boolean GetCSWICtlVal(void);
Boolean SetCSWICtlVal(Boolean inCtlVal);
enum DbPosValues GetCSWIStVal(void);
enum DbPosValues SetCSWIStVal(enum DbPosValues inStVal);

/* GGIO */
Boolean GetGGIOCtlVal(void);
Boolean SetGGIOCtlVal(Boolean inCtlVal);
Boolean GetGGIOStVal(void);
Boolean SetGGIOStVal(Boolean inStVal);

/* TTNS */
Float32 GetTTNSTnsSv1Magf(void);
Float32 GetTTNSTnsSv2Magf(void);
Float32 GetTTNSTnsSv3Magf(void);
Float32 SetTTNSTnsSv1Magf(Float32 inMagf);
Float32 SetTTNSTnsSv2Magf(Float32 inMagf);
Float32 SetTTNSTnsSv3Magf(Float32 inMagf);

/* MMXU */
Integer32 GetMMXUPhsAMagi(void);
Integer32 SetMMXUPhsAMagi(Integer32 inPhsMagi);
Integer32 GetMMXUPhsBMagi(void);
Integer32 SetMMXUPhsBMagi(Integer32 inPhsMagi);
Integer32 GetMMXUPhsCMagi(void);
Integer32 SetMMXUPhsCMagi(Integer32 inPhsMagi);

/* IARC */
Integer32 GetMaxNumRcd(void);
Integer32 GetOpMod(void);
Integer32 GetMemFull(void);
Integer32 SetMaxNumRcd(Integer32 inVal);
Integer32 SetOpMod(Integer32 inVal);
Integer32 SetMemFull(Integer32 inVal);

/* PDIF */
CrvPts GetPDIFCrvPts(Integer8 inIndex);
CrvPts SetPDIFCrvPts(Integer8 inIndex, CrvPts inCrvPts);

/* ErrorString*/
char* GetErrorString(void);
char* SetErrorString(char* inErrorString, Unsigned16 inLength);

#endif
