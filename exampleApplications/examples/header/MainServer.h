/******************************************************************************
*
	\par        Dalian Yunxing Tech Co., Ltd.

				Dalian, China
				Phone   : +86 (411) 8825 4852
				Email   : yx@yunxing.tech
*******************************************************************************/

/*****************************************************************************/
/*!	\file		MainServer.h
 *	\brief 		Header file for the PIS10 Stack Example
	\par        Dalian Yunxing Tech Co., Ltd.

				Dalian, China
				Phone   : +86 (411) 8825 4852
				Email   : yx@yunxing.tech
 */
/*****************************************************************************/

/******************************************************************************
*	Defines
******************************************************************************/

#ifndef MAIN_SERVER_INCLUDED
#define MAIN_SERVER_INCLUDED 1

/******************************************************************************
*	Includes
******************************************************************************/

#include <stdio.h>
#include <stdlib.h>

#include "IEC61850Types.h"
#include "IEC61850API.h"
#include "sysctype.h"

#include "PIS10CreateServerClient.h"
#include "IEC61850Functions.h"
#include "LocalData.h"
#include "ExampleTypes.h"
#include "PrintView.h"
#include "UserInput.h"

/******************************************************************************
*	Prototypes
******************************************************************************/

enum IEC61850_ErrorCodes setupIEC61850Server(void);

#endif
