/******************************************************************************
*
	\par        Dalian Yunxing Tech Co., Ltd.

				Dalian, China
				Phone   : +86 (411) 8825 4852
				Email   : yx@yunxing.tech
*******************************************************************************/

/*****************************************************************************/
/*!	\file		PIS10Callbacks.h
*	\brief 		C header file for the PIS10 Stack Example
	\par        Dalian Yunxing Tech Co., Ltd.

				Dalian, China
				Phone   : +86 (411) 8825 4852
				Email   : yx@yunxing.tech
*/
/*****************************************************************************/

/******************************************************************************
*	Defines
******************************************************************************/


#ifndef CALLBACK_FUNCTIONS_INCLUDED
#define CALLBACK_FUNCTIONS_INCLUDED 1

/******************************************************************************
*	Includes
******************************************************************************/
#include <stdio.h>
#include <math.h>

#include "IEC61850Types.h"
#include "IEC61850API.h"
#include "sysctype.h"

#include "PIS10CreateServerClient.h"
#include "LocalData.h"
#include "PrintView.h"

/******************************************************************************
*	Prototypes
******************************************************************************/


//Read Callback
enum IEC61850_CallbackReturnServiceErrorCodes ReadCallbackHandler( void *ptUserData, const struct IEC61850_DataAttributeID *ptDataAttributeID, struct IEC61850_DataAttributeData *ptReturnedValue);

//Write Callback
enum IEC61850_CallbackReturnServiceErrorCodes  WriteCallbackHandler(void * ptUserData, const struct IEC61850_DataAttributeID * ptDataAttributeID, const struct IEC61850_DataAttributeData * ptNewValue);

//Select Callback                           
enum eCommandAddCause  SelectCallbackHandler(void * ptUserData, const struct IEC61850_DataAttributeID * ptControlID, const struct IEC61850_DataAttributeData * ptSelectValue, const struct IEC61850_CommandParameters* ptSelectParameters);

//Operate Callback
enum eCommandAddCause  OperateCallbackHandler(void * ptUserData, const struct IEC61850_DataAttributeID* ptControlID, const struct IEC61850_DataAttributeData * ptOperateValue, const struct IEC61850_CommandParameters* ptOperateParameters);

//Cancel Callback
enum eCommandAddCause  CancelCallbackHandler( void *ptUserData, const struct IEC61850_DataAttributeID * ptControlID, const struct IEC61850_CommandParameters* ptCancelParameters);

//Update Callback
void  UpdateCallbackHandler( void *ptUserData, const struct IEC61850_DataAttributeID * ptControlID, const struct IEC61850_DataAttributeData *ptNewValue);

//Error Callback
enum eCommandAddCause ErrorCallbackHandler(void * ptUserData, const struct IEC61850_DataAttributeID * ptControlID, const struct IEC61850_ErrorParameters * ptErrorParamtrs);

//Operative Test Callback
enum eCommandAddCause OperativeTestCallbackHandler(void * ptUserData, const struct IEC61850_DataAttributeID * ptControlID, const struct IEC61850_CommandParameters* ptOperativeTestParameters);

//Questionable Data Point Callback
void QuestionableCallbackHandler(void * ptUserData, const struct IEC61850_DataAttributeID * ptDataAttributeID);

//Command Termination Callback
enum eCommandAddCause CommandTerminationCallback(void * ptUserData, const struct IEC61850_DataAttributeID * ptControlID, const struct IEC61850_DataAttributeData * ptCmdTermValue);

enum IEC61850_FileResponseCode FileCallbackHandler(void * ptUserData, enum IEC61850_FileCallType fileCallType, struct tFileAttr* ptFileAttributes);

#endif
