/*****************************************************************************/
/*!	\file		MainServer.c
 *	\brief 		C Source code file for the Example IEC61850 Server using the PIS10 Stack
 *				This server has a CSWI, GGIO, Phase Measurement and a Cure Description Array
				The server publishes GOOSE packets containing the CSWI and GGIO stVal and q values
				Buffered and UnBuffered reports are available for CSWI, GGIO, Phase Measurement and
				a Cure Description Array to the client to get data from the server.
				All points can also bee read by a client.

	\par        Dalian Yunxing Tech Co., Ltd.

				Dalian, China
				Phone   : +86 (411) 8825 4852
				Email   : yx@yunxing.tech
 */
/*****************************************************************************/

#if defined (__PARADIGM__)|| defined(_SC1x5_)
// for BECK we need the clib.h
#include <clib.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "MainServer.h"

int main ()
{
	char continueMainLoop = 'y'; /*Run loop variable */
	enum UserCommands usrCommand = COMMAND_NONE; /* The User command to execute*/
	enum IEC61850_ErrorCodes IEC61850Error = IEC61850_ERROR_NONE; //Variable to hold the IEC61850 Error Codes
	Integer32 int32Val =0; //Temporary value to hold user input
	Integer8 int8Val =0; //Temporary value to hold user input
	Float32 f32Val = 0.0; //Temporary value to hold user input
	CrvPts CrvPtsVal = {0}; //Temporary value to hold user input

#if defined (__PARADIGM__) || defined(_SC1x5_)
	// for BECK Set STDIO focus to application
	BIOS_Set_Focus(FOCUS_APPLICATION);
#endif
	PrintServerHeader();

	IEC61850Error = setupIEC61850Server();

	if(IEC61850Error == IEC61850_ERROR_NONE) //If the server stated successfully
	{
		do
		{
			PrintServerFullView(); /* Initialize the Screen */

			usrCommand = GetServerCommandFromUser(); /* Get the User Input */

			switch(usrCommand)
			{
				case COMMAND_EXIT:
					continueMainLoop = 'n'; /* Set the Loop to exit */
					break;
				case COMMAND_UPDATE_MMXU_PHSA:
					/* Get a value from user */
					int32Val = GetInteger32FromUser();
					/* Do the Update with the value*/
					UpdateMMXUPhsAMagi(int32Val);
					break;
				case COMMAND_UPDATE_MMXU_PHSB:
					/* Get a value from user */
					int32Val = GetInteger32FromUser();
					/* Do the Update with the value*/
					UpdateMMXUPhsBMagi(int32Val);
					break;
				case COMMAND_UPDATE_MMXU_PHSC:
					/* Get a value from user */
					int32Val = GetInteger32FromUser();
					/* Do the Update with the value*/
					UpdateMMXUPhsCMagi(int32Val);
					break;
				case COMMAND_UPDATE_PDIF_XVAL:
					/* Get Index from User */
					int8Val = GetIndexForArray();
					/* Get a value from user */
					f32Val = GetFloat32FromUser();
					/* Get current CrvPts to update */
					CrvPtsVal = GetPDIFCrvPts(int8Val);
					CrvPtsVal.xVal = f32Val;
					/* Do the Update with the value at the index*/
					UpdatePDIFCrvPts(CrvPtsVal, int8Val);
					break;
				case COMMAND_UPDATE_PDIF_YVAL:
					/* Get Index from User */
					int8Val = GetIndexForArray();
					/* Get a value from user */
					f32Val = GetFloat32FromUser();
					/* Get current CrvPts to update */
					CrvPtsVal = GetPDIFCrvPts(int8Val);
					CrvPtsVal.yVal = f32Val;
					/* Do the Update with the value at the index*/
					UpdatePDIFCrvPts(CrvPtsVal, int8Val);
					break;
				case COMMAND_UPDATE_TTNS_TNSSV1:
					/* Get a value from user */
					f32Val = GetFloat32FromUser();
					/* Do the Update with the value*/
					UpdateTTNSTnsSv1Magf(f32Val);
					break;
				case COMMAND_UPDATE_TTNS_TNSSV2:
					/* Get a value from user */
					f32Val = GetFloat32FromUser();
					/* Do the Update with the value*/
					UpdateTTNSTnsSv2Magf(f32Val);
					break;
				case COMMAND_UPDATE_TTNS_TNSSV3:
					/* Get a value from user */
					f32Val = GetFloat32FromUser();
					/* Do the Update with the value*/
					UpdateTTNSTnsSv3Magf(f32Val);
					break;
                default:
                    break;
			}

		} while (continueMainLoop == 'y');
	}

	printf("Stopping IEC61850 Server \n");
	StopMyServerClient(); // Stop server if running
	printf("Freeing IEC61850 Server \n");
	FreeMyServerClient(); // Free server if running

	//Allow the program to pause and let the user see any exit messages
	printf("\nIEC61850 Server Example has ended, press enter to exit\n");
	getchar();

#if defined (__PARADIGM__)|| defined(_SC1x5_)
	// for BECK Set STDIO focus back to both for terminal and Applications
	BIOS_Set_Focus(FOCUS_BOTH);
#endif

	return 0;
}

/******************************************************************************
*       setupIEC61850 Function Definition
******************************************************************************/
/*!  \brief         This function Creates, Loads and Starts the IEC61850 Server
 *
 *   \ingroup       Setup Function
 *
 *   \param[in]     Var     DESC
 *
 *   \return        IEC61850_ERROR_NONE on success,
 *   \return        otherwise the relevant enum IEC61850_ErrorCodes
 ******************************************************************************/
enum IEC61850_ErrorCodes setupIEC61850Server(void)
{
	enum IEC61850_ErrorCodes returnError = IEC61850_ERROR_SERVICE_FAILED;

	if(memcmp(IEC61850_GetLibraryVersion(), PIS10_VERSION, sizeof(PIS10_VERSION)) == 0)
	{
		returnError = CreateServer(IEC61850_OPTION_NONE);

		if(returnError == IEC61850_ERROR_NONE)
		{
			returnError = LoadSCLFile("../cidFiles/server.cid");

			if(returnError == IEC61850_ERROR_NONE)
			{
				returnError = StartMyServerClient();
			}
			else if(returnError == IEC61850_ERROR_LICENCE_INVALID)
			{
				printf("IEC61850 License for this device is invalid\n");
			}
		}
		else if(returnError == IEC61850_ERROR_LICENCE_INVALID)
		{
            printf("IEC61850 License for this device is invalid\n");
		}
	}
	else
	{
		printf("Version Mismatch: PIS10 IEC61850API.h Version = %s , PIS10 Library Version = %s\n",PIS10_VERSION ,IEC61850_GetLibraryVersion());
	}
	return returnError;
}
