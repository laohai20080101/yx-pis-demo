/*****************************************************************************/
/*!	\file		MainClient.c
 *	\brief 		C Source code file for the Example IEC61850 Client using the PIS10 Stack
 *				This Client receives data for a CSWI, GGIO, Phase Measurement and a Cure Description Array
 *				via GOOSE and Reporting.
 *				The Client can Direct Operate the GGIO and use Select before Operate to control the CSWI which
 *				is and enhanced security control.
 *
	\par        Dalian Yunxing Tech Co., Ltd.

				Dalian, China
				Phone   : +86 (411) 8825 4852
				Email   : yx@yunxing.tech
 */
/*****************************************************************************/

#if defined (__PARADIGM__)
// for BECK we need the clib.h
#include <clib.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "MainClient.h"

int main ()
{
	char continueMainLoop = 'y'; /*Run loop variable */
	enum UserCommands usrCommand = COMMAND_NONE; /* The User command to execute*/
	enum IEC61850_ErrorCodes IEC61850Error = IEC61850_ERROR_NONE; //Variable to hold the IEC61850 Error Codes
	Boolean boolVal = FALSE; //Temporary value to hold user input

#if defined (__PARADIGM__)
	// for BECK Set STDIO focus to application
	BIOS_Set_Focus(FOCUS_APPLICATION);
#endif
	PrintClientHeader();

	IEC61850Error = setupIEC61850Client();

	if(IEC61850Error == IEC61850_ERROR_NONE) //If the Client stated successfully
	{
		do
		{
			PrintClientFullView(); /* Initialize the Screen */

			usrCommand = GetClientCommandFromUser(); /* Get the Command from the User */

			switch(usrCommand)
			{
				case COMMAND_EXIT:
					continueMainLoop = 'n'; /* Set the Loop to exit */
					break;
				case COMMAND_OPERATE_CSWI:
					/* Get a value from user */
					boolVal = GetBooleanFromUser();
					/* CSWI is SBO with enhanced Security so we need to do Select first with the value*/
					IEC61850Error = SelectCSWI(boolVal);
					if(IEC61850Error == IEC61850_ERROR_NONE)
					{
						/* Do the Operate with the value*/
						OperateCSWI(boolVal);
					}
					break;
				case COMMAND_OPERATE_GGIO:
					/* Get a value from user */
					boolVal = GetBooleanFromUser();
					/* Do the Operate with the value*/
					OperateGGIO(boolVal);
					break;
				case COMMAND_MMS_GET_CONNECTED_SERVER_LIST:
					MMSGetConnectionsList();
					break;
				case COMMAND_PRINT_FILE_MENU:
					FileOperate();
					break;
				case COMMAND_PRINT_SETTING_GROUP_MENU:
					SittingGroupOperate();
					break;
				case COMMAND_MMS_QUERY_LOG:
					LogOperate();
					break;
				case COMMAND_GET_DATASET_DIR:
					GetDataSetDirectory();
					break;
				case COMMAND_MMS_READ_AND_WRITE:
					MMSReadOrWrite();
					break;
                default:
                    break;
			}

		} while (continueMainLoop == 'y');


	}

	printf("Stopping IEC61850 Client \n");
	StopMyServerClient(); // Stop Client if running
	printf("Freeing IEC61850 Client \n");
	FreeMyServerClient(); // Free Client if running

	//Allow the program to pause and let the user see any exit messages
	printf("\nIEC61850 Client Example has ended, press enter to exit\n");
	getchar();

#if defined (__PARADIGM__)
	// for BECK Set STDIO focus back to both for terminal and Applications
	BIOS_Set_Focus(FOCUS_BOTH);
#endif

	return 0;
}

/******************************************************************************
*       setupIEC61850Client Function Definition
******************************************************************************/
/*!  \brief         This function Creates, Loads and Starts the IEC61850 Client
 *
 *   \ingroup       Setup Function
 *
 *   \param[in]     Var     DESC
 *
 *   \return        IEC61850_ERROR_NONE on success,
 *   \return        otherwise the relevant enum IEC61850_ErrorCodes
 ******************************************************************************/
enum IEC61850_ErrorCodes setupIEC61850Client()
{
	enum IEC61850_ErrorCodes returnError = IEC61850_ERROR_SERVICE_FAILED;

	if(memcmp(IEC61850_GetLibraryVersion(), PIS10_VERSION, sizeof(PIS10_VERSION)) == 0)  //Make sure the API Header version is the same as the Library
	{
		returnError = CreateClient();

		if(returnError == IEC61850_ERROR_NONE)
		{
			returnError = LoadSCLFile("../cidFiles/client.cid");

			if(returnError == IEC61850_ERROR_NONE)
			{
				returnError = SetControlsOrCat();

				returnError = StartMyServerClient();
			}
			else if(returnError == IEC61850_ERROR_LICENCE_INVALID)
			{
				printf("IEC61850 License for this device is invalid\n");
			}
		}
		else if(returnError == IEC61850_ERROR_LICENCE_INVALID)
		{
			printf("IEC61850 License for this device is invalid\n");
		}
	}
	else
	{
		printf("Version Mismatch: PIS10 IEC61850API.h Version = %s , PIS10 Library Version = %s\n",PIS10_VERSION ,IEC61850_GetLibraryVersion());
	}
	return returnError;
}
