/*****************************************************************************/
/*!	\file		UserInput.c
 *	\brief 		C Source code file for the PIS10 Stack Example
	\par        Dalian Yunxing Tech Co., Ltd.

				Dalian, China
				Phone   : +86 (411) 8825 4852
				Email   : yx@yunxing.tech
 */
/*****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "UserInput.h"
#include "IEC61850Types.h"
#include "IEC61850API.h"
#include "sysctype.h"


/******************************************************************************
*       GetCommandFromUser Function Definition
******************************************************************************/
/*!  \brief         This function Gets the command from the user for the server
 *
 *   \ingroup       User Input
 *
 *
 *   \return        Relevant enum UserCommand on success
 *   \return        COMMAND_NONE on failure or invalid input
 ******************************************************************************/
enum UserCommands GetServerCommandFromUser(void)
{
	enum UserCommands returnVal = COMMAND_NONE;
	char userInput[256] = {0}; //Buffer to hold the user input

	if ( fgets(userInput, sizeof(userInput), stdin) != NULL)
	{
		switch(userInput[0])
		{
			case 'x':
			case 'X':
				returnVal = COMMAND_EXIT;
				break;
			case '1':
				returnVal = COMMAND_UPDATE_MMXU_PHSA;
				break;
			case '2':
				returnVal = COMMAND_UPDATE_MMXU_PHSB;
				break;
			case '3':
				returnVal = COMMAND_UPDATE_MMXU_PHSC;
				break;
			case '4':
				returnVal = COMMAND_UPDATE_PDIF_XVAL;
				break;
			case '5':
				returnVal = COMMAND_UPDATE_PDIF_YVAL;
				break;
			case '6':
				returnVal = COMMAND_UPDATE_TTNS_TNSSV1;
				break;
			case '7':
				returnVal = COMMAND_UPDATE_TTNS_TNSSV2;
				break;
			case '8':
				returnVal = COMMAND_UPDATE_TTNS_TNSSV3;
				break;
		}
	}

	return returnVal;
}


/******************************************************************************
*       GetClientCommandFromUser Function Definition
******************************************************************************/
/*!  \brief         This function Gets the command from the user for the Client
 *
 *   \ingroup       User Input
 *
 *
 *   \return        Relevant enum UserCommand on success
 *   \return        COMMAND_NONE on failure or invalid input
 ******************************************************************************/
enum UserCommands GetClientCommandFromUser(void)
{
	enum UserCommands returnVal = COMMAND_NONE;
	char userInput[256] = {0}; //Buffer to hold the user input

	if ( fgets(userInput, sizeof(userInput), stdin) != NULL)
	{
		switch(userInput[0])
		{
			case 'x':
			case 'X':
				returnVal = COMMAND_EXIT;
				break;
			case '1':
				returnVal = COMMAND_OPERATE_CSWI;
				break;
			case '2':
				returnVal = COMMAND_OPERATE_GGIO;
				break;
			case '3':
				returnVal = COMMAND_MMS_GET_CONNECTED_SERVER_LIST;
				break;
			case '4':
				returnVal = COMMAND_PRINT_FILE_MENU;
				break;
			case '5':
				returnVal = COMMAND_PRINT_SETTING_GROUP_MENU;
				break;	
			case '6':
				returnVal = COMMAND_MMS_QUERY_LOG;
				break;
		}
	}

	return returnVal;
}

/******************************************************************************
*       GetBooleanFromUser Function Definition
******************************************************************************/
/*!  \brief         This function prints the Menu and Data View to the screen
 *
 *   \ingroup       User Input
 *
 *
 *   \return        Boolean TRUE or FALSE depending on user input
 ******************************************************************************/
Boolean GetBooleanFromUser(void)
{
	Boolean returnVal = FALSE;
	char userInput[256] = {0}; //Buffer to hold the user input

	printf("Enter Boolean Value, press T for TRUE or press F for FALSE: ");

	if ( fgets(userInput, sizeof(userInput), stdin) != NULL)
	{
		switch(userInput[0]) //Base the return val on the input
		{
			case '0':
			case 'f':
			case 'F':
				returnVal = FALSE;  //0 or f or F means FALSE
				break;
			case '1':
			case 't':
			case 'T':
				returnVal = TRUE;  //1 or t or T means TRUE
				break;
		}
	}

	return returnVal;
}


/******************************************************************************
*       GetInteger32FromUser Function Definition
******************************************************************************/
/*!  \brief         This function Gets an integer32 from the user
 *
 *   \ingroup       User Input
 *
 *
 *   \return        The Integer32 value of the input supplied by the user
 ******************************************************************************/
Integer32 GetInteger32FromUser(void)
{
	Integer32 returnVal = 0;

	printf("Enter an Integer32 value: ");

#ifdef __PARADIGM__
	scanf("%ld", &returnVal); //On BECK an Integer32 is a long int so %ld is needed
#else
	scanf("%d", &returnVal);
#endif
	return returnVal;
}

Integer32 GetCommIndexFromUser(void)
{
	Integer32 returnVal = 0;

	printf("Enter command index: ");

#ifdef __PARADIGM__
	scanf("%ld", &returnVal); //On BECK an Integer32 is a long int so %ld is needed
#else
	scanf("%d", &returnVal);
#endif
	return returnVal;
}

/******************************************************************************
*       GetIndexForArray Function Definition
******************************************************************************/
/*!  \brief         This function Gets an integer8 from the user
 *
 *   \ingroup       User Input
 *
 *
 *   \return        The Integer8 value of the input supplied by the user
 ******************************************************************************/
Integer8 GetIndexForArray(void)
{
	Integer8 returnVal = 0;
	Integer32 tempInt32 =0;

	printf("Enter the index for the Array [0 - %d]: ", SIZE_OF_PDIF_ARRAY-1);

	scanf("%d", &tempInt32); //%d expects a 32 bit Integer

	returnVal = (Integer8)tempInt32; //Type cast the Integer32 to an Integer8

	return returnVal;
}

/******************************************************************************
*       GetFloat32FromUser Function Definition
******************************************************************************/
/*!  \brief         This function Gets an Float32 from the user
 *
 *   \ingroup       User Input
 *
 *
 *   \return        The Float32 value of the input supplied by the user
 ******************************************************************************/
Float32 GetFloat32FromUser(void)
{
	Float32 returnVal = 0;

	printf("Enter a Float32 value: ");

	scanf("%f", &returnVal);

	return returnVal;
}

struct IEC61850_TimeStamp* GetIEC61850TimeFromUser(void)
{
	struct IEC61850_TimeStamp* ptTimeStamp = NULL;

	char userInput[256] = { 0 }; //Buffer to hold the user input
	char* strDateTime = userInput;
	printf("Please input time(yyyymmddHHMMSS):\n");
	setbuf(stdin, NULL);
	if (fgets(userInput, sizeof(userInput), stdin) != NULL)
	{
		size_t nLen = 0;
		nLen = strlen(strDateTime);
		if (nLen >= 14)
		{
			char year[4] = { 0 };
			char month[2] = { 0 };
			char day[2] = { 0 };
			char hour[2] = { 0 };
			char min[2] = { 0 };
			char sec[2] = { 0 };
			char millisec[3] = { 0 };

			memcpy(year, strDateTime, 4);
			memcpy(month, strDateTime += 4, 2);
			memcpy(day, strDateTime += 2, 2);
			memcpy(hour, strDateTime += 2, 2);
			memcpy(min, strDateTime += 2, 2);
			memcpy(sec, strDateTime += 2, 2);
			ptTimeStamp = (struct IEC61850_TimeStamp*)calloc(1, sizeof(struct IEC61850_TimeStamp));
			if (ptTimeStamp != NULL)
			{
				IEC61850_GetIEC61850TimeFromDate(ptTimeStamp, atoi(month), atoi(day), atoi(year), atoi(hour), atoi(min), atoi(sec), atoi(millisec));
			}
		}
	}
	return ptTimeStamp;
}
