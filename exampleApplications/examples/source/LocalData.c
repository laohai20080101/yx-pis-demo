/*****************************************************************************/
/*!	\file		LocalData.c
 *	\brief 		C Source code file for the PIS10 Stack Example
	\par        Dalian Yunxing Tech Co., Ltd.

				Dalian, China
				Phone   : +86 (411) 8825 4852
				Email   : yx@yunxing.tech
 */
/*****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "LocalData.h"

	/* CSWI */
	Boolean CSWICtlVal = FALSE;
	enum DbPosValues CSWIStVal = DBPOS_OFF;

	/* GGIO */
	Boolean GGIOCtlVal = FALSE;
	Boolean GGIOStVal  = FALSE;

	/* TTNS */
	Float32 TTNSTnsSv1Magf = 0;
	Float32 TTNSTnsSv2Magf = 0;
	Float32 TTNSTnsSv3Magf = 0;

	/* MMXU */
	Integer32 MMXUPhsAMagi = 0;
	Integer32 MMXUPhsBMagi = 0;
	Integer32 MMXUPhsCMagi = 0;

	/* IARC */
	Integer32 MaxNumRcd = 0;
	Integer32 OpMod     = 0;
	Integer32 MemFull   = 0;

	/* PDIF */
	CrvPts PDIFCrvPts[SIZE_OF_PDIF_ARRAY] ={{0}};

	/*Error String */
	char strError[SIZE_OF_ERROR_STRING] = {0};

/******************************************************************************
*       GetCSWICtlVal Function Definition
******************************************************************************/
/*!  \brief         This function gets the current value of the CSWICtlVal
 *
 *   \ingroup       View Function
 *
 *   \param[in]     Var     DESC
 *
 *   \return        The current value of the CSWICtlVal
 ******************************************************************************/
Boolean GetCSWICtlVal(void)
{
	return CSWICtlVal;
}

/******************************************************************************
*       SetCSWICtlVal Function Definition
******************************************************************************/
/*!  \brief         This function sets the value of the CSWICtlVal
 *
 *   \ingroup       Mutator
 *
 *   \param[in]     inCtlVal     a Boolean that will be used the value of the CSWICtlVal
 *
 *   \return        The current value of the CSWICtlVal
 ******************************************************************************/
Boolean SetCSWICtlVal(Boolean inCtlVal)
{
	//Validate input and ensure the value is always either True or False
	if(inCtlVal == FALSE)
	{
		CSWICtlVal = FALSE;
	}
	else
	{
		CSWICtlVal = TRUE;
	}

	return CSWICtlVal;
}

/******************************************************************************
*       GetCSWIStlVal Function Definition
******************************************************************************/
/*!  \brief         This function gets the current value of the CSWIStlVal
 *
 *   \ingroup       View Function
 *
 *   \param[in]     Var     DESC
 *
 *   \return        The current value of the CSWIStlVal
 ******************************************************************************/
enum DbPosValues GetCSWIStVal(void)
{
	return CSWIStVal;
}

/******************************************************************************
*       SetCSWIStlVal Function Definition
******************************************************************************/
/*!  \brief         This function sets the value of the CSWIStlVal
 *
 *   \ingroup       Mutator
 *
 *   \param[in]     inStVal     an enum DbPosValues that will be used the value of the CSWIStlVal
 *
 *   \return        The current value of the CSWIStlVal
 ******************************************************************************/
enum DbPosValues SetCSWIStVal(enum DbPosValues inStVal)
{
	//Validate input to make sure it is a valid value for a DBPos
	switch(inStVal)
	{
		case DBPOS_INTERMEDIATE:
			CSWIStVal = DBPOS_INTERMEDIATE;
			break;
		case DBPOS_OFF:
			CSWIStVal = DBPOS_OFF;
			break;
		case DBPOS_ON:
			CSWIStVal = DBPOS_ON;
			break;
		case DBPOS_BAD:
			CSWIStVal = DBPOS_BAD;
			break;
	}

	return CSWIStVal;
}




/******************************************************************************
*       GetGGIOCtlVal Function Definition
******************************************************************************/
/*!  \brief         This function gets the current value of the GGIOCtlVal
 *
 *   \ingroup       View Function
 *
 *   \param[in]     Var     DESC
 *
 *   \return        The current value of the GGIOCtlVal
 ******************************************************************************/
Boolean GetGGIOCtlVal(void)
{
	return GGIOCtlVal;
}

/******************************************************************************
*       SetGGIOCtlVal Function Definition
******************************************************************************/
/*!  \brief         This function sets the value of the GGIOCtlVal
 *
 *   \ingroup       Mutator
 *
 *   \param[in]     inCtlVal     a Boolean that will be used the value of the GGIOCtlVal
 *
 *   \return        The current value of the GGIOCtlVal
 ******************************************************************************/
Boolean SetGGIOCtlVal(Boolean inCtlVal)
{
	//Validate input and ensure the value is always either True or False
	if(inCtlVal == FALSE)
	{
		GGIOCtlVal = FALSE;
	}
	else
	{
		GGIOCtlVal = TRUE;
	}

	return GGIOCtlVal;
}

/******************************************************************************
*       GetGGIOStVal Function Definition
******************************************************************************/
/*!  \brief         This function gets the current value of the GGIOStVal
 *
 *   \ingroup       Accessor
 *
 *   \param[in]     Var     DESC
 *
 *   \return        The current value of the GGIOCtlVal
 ******************************************************************************/
Boolean GetGGIOStVal(void)
{
	return GGIOStVal;
}

/******************************************************************************
*       SetGGIOStVal Function Definition
******************************************************************************/
/*!  \brief         This function sets the value of the GGIOCtlVal
 *
 *   \ingroup       Mutator
 *
 *   \param[in]     inStVal     a Boolean that will be used the value of the GGIOCtlVal
 *
 *   \return        The current value of the GGIOCtlVal
 ******************************************************************************/
Boolean SetGGIOStVal(Boolean inStVal)
{
	//Validate input and ensure the value is always either True or False
	if(inStVal == FALSE)
	{
		GGIOStVal = FALSE;
	}
	else
	{
		GGIOStVal = TRUE;
	}

	return GGIOStVal;
}

//TTNS
Float32 GetTTNSTnsSv1Magf(void)
{
	return TTNSTnsSv1Magf;
}
Float32 GetTTNSTnsSv2Magf(void)
{
	return TTNSTnsSv2Magf;
}
Float32 GetTTNSTnsSv3Magf(void)
{
	return TTNSTnsSv3Magf;
}
Float32 SetTTNSTnsSv1Magf(Float32 inMagf)
{
	TTNSTnsSv1Magf = inMagf;
	return TTNSTnsSv1Magf;
}
Float32 SetTTNSTnsSv2Magf(Float32 inMagf)
{
	TTNSTnsSv2Magf = inMagf;
	return TTNSTnsSv2Magf;
}
Float32 SetTTNSTnsSv3Magf(Float32 inMagf)
{
	TTNSTnsSv3Magf = inMagf;
	return TTNSTnsSv3Magf;
}

//IARC
Integer32 GetMaxNumRcd(void)
{
	return MaxNumRcd;
}
Integer32 GetOpMod(void)
{
	return OpMod;
}
Integer32 GetMemFull(void)
{
	return MemFull;
}

Integer32 SetMaxNumRcd(Integer32 inVal)
{
	MaxNumRcd = inVal;

	return MaxNumRcd;
}
Integer32 SetOpMod(Integer32 inVal)
{
	OpMod = inVal;

	return OpMod;
}
Integer32 SetMemFull(Integer32 inVal)
{
	MemFull = inVal;

	return MemFull;
}


/******************************************************************************
*       GetMMXUPhsAMagi Function Definition
******************************************************************************/
/*!  \brief         This function gets the current value of the MMXUPhsAMagi
 *
 *   \ingroup       Accessor
 *
 *   \param[in]     Var     DESC
 *
 *   \return        The current value of the MMXUPhsAMagi
 ******************************************************************************/
Integer32 GetMMXUPhsAMagi(void)
{
	return MMXUPhsAMagi;
}

/******************************************************************************
*       SetMMXUPhsAMagi Function Definition
******************************************************************************/
/*!  \brief         This function sets the value of the MMXUPhsAMagi
 *
 *   \ingroup       Mutator
 *
 *   \param[in]     inPhsMagi     an Integer32 that will be used the value of the MMXUPhsAMagi
 *
 *   \return        The current value of the MMXUPhsAMagi
 ******************************************************************************/
Integer32 SetMMXUPhsAMagi(Integer32 inPhsMagi)
{
	MMXUPhsAMagi = inPhsMagi;

	return MMXUPhsAMagi;
}

/******************************************************************************
*       GetMMXUPhsBMagi Function Definition
******************************************************************************/
/*!  \brief         This function gets the current value of the MMXUPhsBMagi
 *
 *   \ingroup       Accessor
 *
 *   \param[in]     Var     DESC
 *
 *   \return        The current value of the MMXUPhsBMagi
 ******************************************************************************/
Integer32 GetMMXUPhsBMagi(void)
{
	return MMXUPhsBMagi;
}

/******************************************************************************
*       SetMMXUPhsAMagi Function Definition
******************************************************************************/
/*!  \brief         This function sets the value of the MMXUPhsBMagi
 *
 *   \ingroup       Mutator
 *
 *   \param[in]     inPhsMagi     an Integer32 that will be used the value of the MMXUPhsBMagi
 *
 *   \return        The current value of the MMXUPhsBMagi
 ******************************************************************************/
Integer32 SetMMXUPhsBMagi(Integer32 inPhsMagi)
{
	MMXUPhsBMagi = inPhsMagi;

	return MMXUPhsBMagi;
}


/******************************************************************************
*       GetMMXUPhsCMagi Function Definition
******************************************************************************/
/*!  \brief         This function gets the current value of the MMXUPhsCMagi
 *
 *   \ingroup       Accessor
 *
 *   \param[in]     Var     DESC
 *
 *   \return        The current value of the MMXUPhsCMagi
 ******************************************************************************/
Integer32 GetMMXUPhsCMagi(void)
{
	return MMXUPhsCMagi;
}

/******************************************************************************
*       SetMMXUPhsAMagi Function Definition
******************************************************************************/
/*!  \brief         This function sets the value of the MMXUPhsCMagi
 *
 *   \ingroup       Mutator
 *
 *   \param[in]     inPhsMagi     an Integer32 that the value of the MMXUPhsCMagi will be set to
 *
 *   \return        The current value of the MMXUPhsCMagi
 ******************************************************************************/
Integer32 SetMMXUPhsCMagi(Integer32 inPhsMagi)
{
	MMXUPhsCMagi = inPhsMagi;

	return MMXUPhsCMagi;
}


/******************************************************************************
*       GetPDIFCrvPts Function Definition
******************************************************************************/
/*!  \brief         This function gets the current value of the PDIFCrvPts at the specified index
 *
 *   \ingroup       Accessor
 *
 *   \param[in]     inIndex     An Integer8 that holds the index to get, between 0 and SIZE_OF_PDIF_ARRAY-1
 *
 *   \return        The current value of the PDIFCrvPts at the specified index
 ******************************************************************************/
CrvPts GetPDIFCrvPts(Integer8 inIndex)
{
	CrvPts returnVal = {0};

	//Make sure the index is not out of range
	if((inIndex >= 0) && (inIndex < SIZE_OF_PDIF_ARRAY))
	{
		returnVal.xVal = PDIFCrvPts[inIndex].xVal;
		returnVal.yVal = PDIFCrvPts[inIndex].yVal;
	}

	return returnVal;
}


/******************************************************************************
*       SetPDIFCrvPts Function Definition
******************************************************************************/
/*!  \brief         This function gets the current value of the PDIFCrvPtsYVal at the specified index
 *
 *   \ingroup       Mutator
 *
 *   \param[in]     inIndex     An Integer8 that holds the index to get, between 0 and SIZE_OF_PDIF_ARRAY-1
 *   \param[in]     inCrvPts    A crvPts structure that holds the new Xval and Yval for the PDIFCrvPts
 *
 *   \return        The current value of the PDIFCrvPts
 ******************************************************************************/
CrvPts SetPDIFCrvPts(Integer8 inIndex, CrvPts inCrvPts)
{
	CrvPts returnVal = {0};

	//Make sure the index is not out of range
	if((inIndex >= 0) && (inIndex < SIZE_OF_PDIF_ARRAY))
	{
		PDIFCrvPts[inIndex].xVal = inCrvPts.xVal;
		PDIFCrvPts[inIndex].yVal = inCrvPts.yVal;

		returnVal.xVal = PDIFCrvPts[inIndex].xVal;
		returnVal.yVal = PDIFCrvPts[inIndex].yVal;
	}

	return returnVal;
}



/******************************************************************************
*       GetErrorString Function Definition
******************************************************************************/
/*!  \brief         This function gets the current Error String
 *
 *   \ingroup       Accessor
 *
 *   \param[in]     Var     DESC
 *
 *   \return        The current value of the Error String
 ******************************************************************************/
char* GetErrorString(void)
{
	return strError;
}

/******************************************************************************
*       SetErrorString Function Definition
******************************************************************************/
/*!  \brief         This function sets the value of the MMXUPhsCMagi
 *
 *   \ingroup       Mutator
 *
 *   \param[in]     inErrorString     A C string that contains the new error string
 *   \param[in]     inLength		  An Unsigned8 that holds the length of the new error string, must be less than SIZE_OF_ERROR_STRING
 *
 *   \return        The current value of the MMXUPhsCMagi
 ******************************************************************************/
char* SetErrorString(char* inErrorString, Unsigned16 inLength)
{
	char * returnVal = NULL;

	/* Validate inputs */
	if((inErrorString != NULL) && ((inLength <= SIZE_OF_ERROR_STRING)))
	{
		memset(strError, 0, SIZE_OF_ERROR_STRING); //Set the string to 0

		memcpy(strError, inErrorString, inLength); //Copy in the new Error String
	}

	return returnVal;
}



