/*****************************************************************************/
/*!	\file		MainServer.c
 *	\brief 		C Source code file for the Example IEC61850 Server GOOSE Subscription using the PIS10 Stack
 *				This server has a CSWI and GGIO where the stVal's of each holds the same value as the example server
 *				This is done by having the server subscribe to the GOOSE packets being sent from the YUNXING IEC61850 Server Example
 *				These points are linked using External References in the CID file.
 *
	\par        Dalian Yunxing Tech Co., Ltd.

				Dalian, China
				Phone   : +86 (411) 8825 4852
				Email   : yx@yunxing.tech
 */
/*****************************************************************************/

#if defined (__PARADIGM__)
// for BECK we need the clib.h
#include <clib.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "MainServer.h"

int main ()
{
	char continueMainLoop = 'y'; /*Run loop variable */
	enum UserCommands usrCommand = COMMAND_NONE; /* The User command to execute*/
	enum IEC61850_ErrorCodes IEC61850Error = IEC61850_ERROR_NONE; //Variable to hold the IEC61850 Error Codes
	Boolean boolVal = TRUE; //Value to set as user data


#if defined (__PARADIGM__)
	// for BECK Set STDIO focus to application
	BIOS_Set_Focus(FOCUS_APPLICATION);
#endif
	PrintServerSubscriptionHeader(); 

	IEC61850Error = setupIEC61850Server();

	if(IEC61850Error == IEC61850_ERROR_NONE) //If the server stated successfully
	{
		SetUserData((void*)&boolVal); // Set user data to be used in the callbacks

		do
		{
			PrintServerSubscriptionFullView(); /* Initialize the Screen */

			usrCommand = GetServerCommandFromUser(); /* Get the User Input */

			switch(usrCommand)
			{
				case COMMAND_EXIT:
					continueMainLoop = 'n'; /* Set the Loop to exit */
					break;
                default:
                    break;
			}

		} while (continueMainLoop == 'y');
	}

	printf("Stopping IEC61850 Server \n");
	StopMyServerClient(); // Stop server if running
	printf("Freeing IEC61850 Server \n");
	FreeMyServerClient(); // Free server if running

	//Allow the program to pause and let the user see any exit messages
	printf("\nIEC61850 Server Example has ended, press enter to exit\n");
	getchar();

#if defined (__PARADIGM__)
	// for BECK Set STDIO focus back to both for terminal and Applications
	BIOS_Set_Focus(FOCUS_BOTH);
#endif

	return 0;
}

/******************************************************************************
*       setupIEC61850 Function Definition
******************************************************************************/
/*!  \brief         This function Creates, Loads and Starts the IEC61850 Server
 *
 *   \ingroup       Setup Function
 *
 *   \param[in]     Var     DESC
 *
 *   \return        IEC61850_ERROR_NONE on success,
 *   \return        otherwise the relevant enum IEC61850_ErrorCodes
 ******************************************************************************/
enum IEC61850_ErrorCodes setupIEC61850Server()
{
	enum IEC61850_ErrorCodes returnError = IEC61850_ERROR_SERVICE_FAILED;

	if(memcmp(IEC61850_GetLibraryVersion(), PIS10_VERSION, sizeof(PIS10_VERSION)) == 0)
	{
		returnError = CreateServer(IEC61850_OPTION_SERVER_SUBSCRIBE_GOOSE | IEC61850_OPTION_ALLOW_DUPLICATE_DAID);

		if(returnError == IEC61850_ERROR_NONE)
		{
			returnError = LoadSCLFile("../cidFiles/serverSubscriber.cid");

			if(returnError == IEC61850_ERROR_NONE)
			{
				returnError = StartMyServerClient();
			}
			else if(returnError == IEC61850_ERROR_LICENCE_INVALID)
			{
				printf("IEC61850 License for this device is invalid\n");
			}
		}
		else if(returnError == IEC61850_ERROR_LICENCE_INVALID)
		{
            printf("IEC61850 License for this device is invalid\n");
		}
	}
	else
	{
		printf("Version Mismatch: PIS10 IEC61850API.h Version = %s , PIS10 Library Version = %s\n",PIS10_VERSION ,IEC61850_GetLibraryVersion());
	}
	return returnError;
}
